import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { ThanxComponent } from './thanx.component';
import { AddedRegistrationComponent } from './added-registration/added-registration.component';

const routes: Routes = [
  {path: '', component: FormComponent},
  {path: 'thanx', component: ThanxComponent},
  {path: 'added-registration', component: AddedRegistrationComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

