import { Subject } from 'rxjs';
import { Register } from './register.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';

@Injectable()

export class RegisterService {
  registerChange = new Subject<Register[]>();
  registerFetching = new Subject<boolean>();
  registerUploading = new Subject<boolean>();
  registerRemoving = new Subject<boolean>();

  constructor(private http: HttpClient){}
  private registers: Register[]= [];

  fetchRegisters(){
    this.registerFetching.next(true);
    this.http.get<{[id: string]: Register}>('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/registers.json')
      .pipe(map(result =>{
        return Object.keys(result).map(id =>{
          const registerData = result[id];
          return new Register(
            id,
            registerData.firstName,
            registerData.lastName,
            registerData.sureName,
            registerData.size,
            registerData.phoneNumber,
            registerData.placeOfStudy,
            registerData.gender,
            registerData.comment,
            registerData.skills,
          );
        });
      }))
      .subscribe(registers =>{
        this.registers = registers;
        this.registerChange.next(this.registers.slice());
        this.registerFetching.next(false);
      }, () =>{
        this.registerFetching.next(false);
      });
  }

  addRegister(register: Register){
    const body = {
      firstName: register.firstName,
      lastName: register.lastName,
      sureName: register.sureName,
      size: register.size,
      phoneNumber: register.phoneNumber,
      placeOfStudy: register.placeOfStudy,
      gender: register.gender,
      comment: register.comment,
      skills: register.skills,
    };
    this.registerUploading.next(true);

    return this.http.post('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/registers.json', body)
      .pipe(tap( () =>{
          this.registerUploading.next(false);
        }, () =>{
          this.registerUploading.next(false);
        })
      );
  }

  removeRegister(id: string){
    this.registerRemoving.next(true);

    return this.http.delete(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/registers/${id}.json`)
      .pipe(tap( () =>{
        this.registerRemoving.next(false);
      }, () =>{
        this.registerRemoving.next(false);
      }))

  }
}
