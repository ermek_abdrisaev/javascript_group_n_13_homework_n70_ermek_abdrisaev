export class Register {
  constructor(
    public id: string,
    public firstName: string,
    public lastName: string,
    public sureName: string,
    public size: string,
    public phoneNumber: string,
    public placeOfStudy: string,
    public gender: string,
    public comment: string,
    public skills: Skills[],
  ){}
}

export class Skills{
  constructor(
    public skill: string,
    public level: string,
  ){}
}
