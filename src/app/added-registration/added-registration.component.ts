import { Component, OnDestroy, OnInit } from '@angular/core';
import { Register } from '../shared/register.model';
import { RegisterService } from '../shared/register.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-added-registration',
  templateUrl: './added-registration.component.html',
  styleUrls: ['./added-registration.component.css']
})
export class AddedRegistrationComponent implements OnInit, OnDestroy {
  registers!: Register[];
  registersChangeSubscription!: Subscription;
  registersFetchingSubscription!: Subscription;
  loading: boolean = false;

  constructor(private registerService: RegisterService) { }

  ngOnInit(): void {
    this.registersChangeSubscription = this.registerService.registerChange.subscribe((registers: Register[]) =>{
      this.registers =registers.reverse();
      this.registers.forEach(item => {
      })
    });
    this.registersFetchingSubscription = this.registerService.registerFetching.subscribe((isFetching: boolean) =>{
      this.loading = isFetching;
    });
    this.registerService.fetchRegisters();
  }

  onDeleteRegister(id: string){
      this.registerService.removeRegister(id).subscribe(() =>{
        this.registerService.fetchRegisters();
      })
    }

  ngOnDestroy(){
    this.registersChangeSubscription.unsubscribe();
    this.registersFetchingSubscription.unsubscribe();
  }
}
