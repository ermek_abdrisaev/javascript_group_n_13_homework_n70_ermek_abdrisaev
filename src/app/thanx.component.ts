import { Component } from '@angular/core';

@Component({
  selector: 'app-thanx',
  template: `<h1 class="mt-5 ml-5">Thank you for registration. We will respond as soon as possible</h1>,
  <a class="btn btn-success ml-5 mt-3" routerLink="/">Go back to registers</a>`,
  styles: [`h1{color: blue;}`, `a{padding: 10px}{text-align: center}`],
})

export class ThanxComponent{}
