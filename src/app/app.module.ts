import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegisterService } from './shared/register.service';
import { FormComponent } from './form/form.component';
import { ValidationPhoneNumberDirective } from './validate-number.directive';
import { AddedRegistrationComponent } from './added-registration/added-registration.component';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ValidationPhoneNumberDirective,
    AddedRegistrationComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
