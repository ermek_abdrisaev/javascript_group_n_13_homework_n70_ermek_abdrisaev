import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { phoneNumberValidator } from '../validate-number.directive';
import { RegisterService } from '../shared/register.service';
import { Router } from '@angular/router';
import { Register } from '../shared/register.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  profileForm!: FormGroup;
  isUploading = false;
  registerUploadingSubscription!: Subscription;
  isTouched = true;

  constructor(
    private registerService: RegisterService,
    private router: Router,) {}

  ngOnInit(): void {
    this.registerUploadingSubscription = this.registerService.registerUploading.subscribe((isUploading: boolean) =>{
      this.isUploading = isUploading;
    });

    this.profileForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      sureName: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', [Validators.required, Validators.minLength(8), phoneNumberValidator]),
      placeOfStudy: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      comment: new FormControl('', Validators.required),
      skills: new FormArray([]),
    });
  }

  onSubmit(){
    const id = Math.random().toString();
    const register = new Register(
      id,
      this.profileForm.value.firstName,
      this.profileForm.value.lastName,
      this.profileForm.value.sureName,
      this.profileForm.value.size,
      this.profileForm.value.phoneNumber,
      this.profileForm.value.placeOfStudy,
      this.profileForm.value.gender,
      this.profileForm.value.comment,
      this.profileForm.value.skills,
    );

    this.registerService.addRegister(register).subscribe();
    void this.router.navigate(['/thanx']);
  }

  addSkillsAndLevel() {
    this.isTouched = false;
    const skills = <FormArray>this.profileForm.get('skills');
    const skillGroup = new FormGroup({
      skill: new FormControl('', Validators.required),
      level: new FormControl('', Validators.required),
    });
    skills.push(skillGroup);
  }

  getSkillControls() {
    const skills = <FormArray>(this.profileForm.get('skills'));
    return skills.controls;
  }

  fieldHasError(fieldName: string, errorType: string){
    const field = this.profileForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }
}
