import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive } from '@angular/core';


export const phoneNumberValidator = () : ValidatorFn =>{
  return (control: AbstractControl): ValidationErrors | null =>{
    const hasNumbers = /^[6-9]{3} [0-9]{3} [0-9]{3}-[0-9]{3}$/.test(control.value);
    if(hasNumbers){
      return null;
    }
    return {phoneNumber: true};
  }
};

@Directive({
  selector: '[appNumber]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidationPhoneNumberDirective,
    multi: true,
  }]
})

export class  ValidationPhoneNumberDirective implements Validator{
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneNumberValidator()(control);
  }
}
